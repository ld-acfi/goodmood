<?php

namespace ContainerR4uxUFC;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder49dfa = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer346ef = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties10b64 = [
        
    ];

    public function getConnection()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getConnection', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getMetadataFactory', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getExpressionBuilder', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'beginTransaction', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getCache', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getCache();
    }

    public function transactional($func)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'transactional', array('func' => $func), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->transactional($func);
    }

    public function commit()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'commit', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->commit();
    }

    public function rollback()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'rollback', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getClassMetadata', array('className' => $className), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'createQuery', array('dql' => $dql), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'createNamedQuery', array('name' => $name), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'createQueryBuilder', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'flush', array('entity' => $entity), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'clear', array('entityName' => $entityName), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->clear($entityName);
    }

    public function close()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'close', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->close();
    }

    public function persist($entity)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'persist', array('entity' => $entity), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'remove', array('entity' => $entity), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'refresh', array('entity' => $entity), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'detach', array('entity' => $entity), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'merge', array('entity' => $entity), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getRepository', array('entityName' => $entityName), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'contains', array('entity' => $entity), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getEventManager', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getConfiguration', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'isOpen', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getUnitOfWork', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getProxyFactory', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'initializeObject', array('obj' => $obj), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'getFilters', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'isFiltersStateClean', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'hasFilters', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return $this->valueHolder49dfa->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer346ef = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder49dfa) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder49dfa = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder49dfa->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, '__get', ['name' => $name], $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        if (isset(self::$publicProperties10b64[$name])) {
            return $this->valueHolder49dfa->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49dfa;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder49dfa;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49dfa;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder49dfa;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, '__isset', array('name' => $name), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49dfa;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder49dfa;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, '__unset', array('name' => $name), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49dfa;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder49dfa;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, '__clone', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        $this->valueHolder49dfa = clone $this->valueHolder49dfa;
    }

    public function __sleep()
    {
        $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, '__sleep', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;

        return array('valueHolder49dfa');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer346ef = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer346ef;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer346ef && ($this->initializer346ef->__invoke($valueHolder49dfa, $this, 'initializeProxy', array(), $this->initializer346ef) || 1) && $this->valueHolder49dfa = $valueHolder49dfa;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder49dfa;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder49dfa;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
