<?php

namespace Symfony\Config;


use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;


/**
 * This class is automatically generated to help creating config.
 *
 * @experimental in 5.3
 */
class GesdinetJwtRefreshTokenConfig implements \Symfony\Component\Config\Builder\ConfigBuilderInterface
{
    private $ttl;
    private $ttlUpdate;
    private $firewall;
    private $userProvider;
    private $userIdentityField;
    private $managerType;
    private $refreshTokenClass;
    private $objectManager;
    private $userChecker;
    private $refreshTokenEntity;
    private $entityManager;
    private $singleUse;
    private $tokenParameterName;
    private $doctrineMappings;
    
    /**
     * @default 2592000
     * @param ParamConfigurator|int $value
     * @return $this
     */
    public function ttl($value): self
    {
        $this->ttl = $value;
    
        return $this;
    }
    
    /**
     * @default false
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function ttlUpdate($value): self
    {
        $this->ttlUpdate = $value;
    
        return $this;
    }
    
    /**
     * @default 'api'
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function firewall($value): self
    {
        $this->firewall = $value;
    
        return $this;
    }
    
    /**
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function userProvider($value): self
    {
        $this->userProvider = $value;
    
        return $this;
    }
    
    /**
     * @default 'username'
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function userIdentityField($value): self
    {
        $this->userIdentityField = $value;
    
        return $this;
    }
    
    /**
     * Set manager mode instead of default one (orm)
     * @default 'orm'
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function managerType($value): self
    {
        $this->managerType = $value;
    
        return $this;
    }
    
    /**
     * Set another refresh token class to use instead of default one (Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken)
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function refreshTokenClass($value): self
    {
        $this->refreshTokenClass = $value;
    
        return $this;
    }
    
    /**
     * Set object manager to use (default: doctrine.orm.entity_manager)
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function objectManager($value): self
    {
        $this->objectManager = $value;
    
        return $this;
    }
    
    /**
     * @default 'security.user_checker'
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function userChecker($value): self
    {
        $this->userChecker = $value;
    
        return $this;
    }
    
    /**
     * Deprecated, use refresh_token_class instead
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function refreshTokenEntity($value): self
    {
        $this->refreshTokenEntity = $value;
    
        return $this;
    }
    
    /**
     * Deprecated, use object_manager instead
     * @default null
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function entityManager($value): self
    {
        $this->entityManager = $value;
    
        return $this;
    }
    
    /**
     * When true, generate a new refresh token on consumption (deleting the old one)
     * @default false
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function singleUse($value): self
    {
        $this->singleUse = $value;
    
        return $this;
    }
    
    /**
     * @default 'refresh_token'
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function tokenParameterName($value): self
    {
        $this->tokenParameterName = $value;
    
        return $this;
    }
    
    /**
     * When true, resolving of Doctrine mapping is done automatically to use either ORM or ODM object manager
     * @default true
     * @param ParamConfigurator|bool $value
     * @return $this
     */
    public function doctrineMappings($value): self
    {
        $this->doctrineMappings = $value;
    
        return $this;
    }
    
    public function getExtensionAlias(): string
    {
        return 'gesdinet_jwt_refresh_token';
    }
            
    
    public function __construct(array $value = [])
    {
    
        if (isset($value['ttl'])) {
            $this->ttl = $value['ttl'];
            unset($value['ttl']);
        }
    
        if (isset($value['ttl_update'])) {
            $this->ttlUpdate = $value['ttl_update'];
            unset($value['ttl_update']);
        }
    
        if (isset($value['firewall'])) {
            $this->firewall = $value['firewall'];
            unset($value['firewall']);
        }
    
        if (isset($value['user_provider'])) {
            $this->userProvider = $value['user_provider'];
            unset($value['user_provider']);
        }
    
        if (isset($value['user_identity_field'])) {
            $this->userIdentityField = $value['user_identity_field'];
            unset($value['user_identity_field']);
        }
    
        if (isset($value['manager_type'])) {
            $this->managerType = $value['manager_type'];
            unset($value['manager_type']);
        }
    
        if (isset($value['refresh_token_class'])) {
            $this->refreshTokenClass = $value['refresh_token_class'];
            unset($value['refresh_token_class']);
        }
    
        if (isset($value['object_manager'])) {
            $this->objectManager = $value['object_manager'];
            unset($value['object_manager']);
        }
    
        if (isset($value['user_checker'])) {
            $this->userChecker = $value['user_checker'];
            unset($value['user_checker']);
        }
    
        if (isset($value['refresh_token_entity'])) {
            $this->refreshTokenEntity = $value['refresh_token_entity'];
            unset($value['refresh_token_entity']);
        }
    
        if (isset($value['entity_manager'])) {
            $this->entityManager = $value['entity_manager'];
            unset($value['entity_manager']);
        }
    
        if (isset($value['single_use'])) {
            $this->singleUse = $value['single_use'];
            unset($value['single_use']);
        }
    
        if (isset($value['token_parameter_name'])) {
            $this->tokenParameterName = $value['token_parameter_name'];
            unset($value['token_parameter_name']);
        }
    
        if (isset($value['doctrine_mappings'])) {
            $this->doctrineMappings = $value['doctrine_mappings'];
            unset($value['doctrine_mappings']);
        }
    
        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }
    
    
    public function toArray(): array
    {
        $output = [];
        if (null !== $this->ttl) {
            $output['ttl'] = $this->ttl;
        }
        if (null !== $this->ttlUpdate) {
            $output['ttl_update'] = $this->ttlUpdate;
        }
        if (null !== $this->firewall) {
            $output['firewall'] = $this->firewall;
        }
        if (null !== $this->userProvider) {
            $output['user_provider'] = $this->userProvider;
        }
        if (null !== $this->userIdentityField) {
            $output['user_identity_field'] = $this->userIdentityField;
        }
        if (null !== $this->managerType) {
            $output['manager_type'] = $this->managerType;
        }
        if (null !== $this->refreshTokenClass) {
            $output['refresh_token_class'] = $this->refreshTokenClass;
        }
        if (null !== $this->objectManager) {
            $output['object_manager'] = $this->objectManager;
        }
        if (null !== $this->userChecker) {
            $output['user_checker'] = $this->userChecker;
        }
        if (null !== $this->refreshTokenEntity) {
            $output['refresh_token_entity'] = $this->refreshTokenEntity;
        }
        if (null !== $this->entityManager) {
            $output['entity_manager'] = $this->entityManager;
        }
        if (null !== $this->singleUse) {
            $output['single_use'] = $this->singleUse;
        }
        if (null !== $this->tokenParameterName) {
            $output['token_parameter_name'] = $this->tokenParameterName;
        }
        if (null !== $this->doctrineMappings) {
            $output['doctrine_mappings'] = $this->doctrineMappings;
        }
    
        return $output;
    }
    

}
