<?php

namespace App\Controller\Admin;

use App\Entity\DailyActivity;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class DailyActivityCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return DailyActivity::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            DateField::new('created_at'),
            AssociationField::new('activity'),
            AssociationField::new('dailyMood'),


        ];
    }

}
