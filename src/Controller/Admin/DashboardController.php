<?php

namespace App\Controller\Admin;

use App\Entity\Activity;
use App\Entity\ActivityPicture;
use App\Entity\DailyActivity;
use App\Entity\DailyMood;
use App\Entity\Mood;
use App\Entity\MoodPicture;
use App\Entity\Note;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;


class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {

        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(UserCrudController::class)->generateUrl());
    }


    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Mood Board Dashboard');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Activités', 'fas fa-list', Activity::class);
        yield MenuItem::linkToCrud('Activités images', 'fas fa-list', ActivityPicture::class);
        yield MenuItem::linkToCrud('Daily Activities', 'fas fa-book', DailyActivity::class);
        yield MenuItem::linkToCrud('Mood', 'fas fa-gift', Mood::class);
        yield MenuItem::linkToCrud('Daily Mood', 'fas fa-gift', DailyMood::class);
        yield MenuItem::linkToCrud('Mood Picture', 'fas fa-gift', MoodPicture::class);
        yield MenuItem::linkToCrud('Note', 'fas fa-tag', Note::class);

    }
}

