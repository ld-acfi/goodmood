<?php

namespace App\Controller\Admin;

use App\Entity\Mood;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MoodCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Mood::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
