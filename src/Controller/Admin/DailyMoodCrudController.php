<?php

namespace App\Controller\Admin;

use App\Entity\DailyMood;
use Doctrine\DBAL\Types\DateImmutableType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class DailyMoodCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return DailyMood::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('user'),
            AssociationField::new('mood'),
            DateField::new('created_at')
        ];
    }

}
