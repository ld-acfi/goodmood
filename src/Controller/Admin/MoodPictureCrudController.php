<?php

namespace App\Controller\Admin;

use App\Entity\MoodPicture;
use Doctrine\DBAL\Types\DateTimeType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class MoodPictureCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return MoodPicture::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            ImageField::new('path')->setBasePath('uploads/')->setUploadDir('public/uploads/')->setUploadedFileNamePattern('[randomhash].[extension]'),
            DateField::new('created_at'),
            AssociationField::new('mood')

        ];
    }

}
