<?php

namespace App\Repository;

use App\Entity\MoodPicture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MoodPicture|null find($id, $lockMode = null, $lockVersion = null)
 * @method MoodPicture|null findOneBy(array $criteria, array $orderBy = null)
 * @method MoodPicture[]    findAll()
 * @method MoodPicture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MoodPictureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MoodPicture::class);
    }

    // /**
    //  * @return MoodPicture[] Returns an array of MoodPicture objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MoodPicture
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
