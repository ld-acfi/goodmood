<?php

namespace App\Repository;

use App\Entity\DailyActivity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DailyActivity|null find($id, $lockMode = null, $lockVersion = null)
 * @method DailyActivity|null findOneBy(array $criteria, array $orderBy = null)
 * @method DailyActivity[]    findAll()
 * @method DailyActivity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DailyActivityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DailyActivity::class);
    }

    // /**
    //  * @return DailyActivity[] Returns an array of DailyActivity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DailyActivity
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
