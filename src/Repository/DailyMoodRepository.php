<?php

namespace App\Repository;

use App\Entity\DailyMood;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DailyMood|null find($id, $lockMode = null, $lockVersion = null)
 * @method DailyMood|null findOneBy(array $criteria, array $orderBy = null)
 * @method DailyMood[]    findAll()
 * @method DailyMood[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DailyMoodRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DailyMood::class);
    }

    // /**
    //  * @return DailyMood[] Returns an array of DailyMood objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DailyMood
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
