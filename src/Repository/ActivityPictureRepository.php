<?php

namespace App\Repository;

use App\Entity\ActivityPicture;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ActivityPicture|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActivityPicture|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActivityPicture[]    findAll()
 * @method ActivityPicture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActivityPictureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ActivityPicture::class);
    }

    // /**
    //  * @return ActivityPicture[] Returns an array of ActivityPicture objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ActivityPicture
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
