<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

/*    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }*/

    public function load(ObjectManager $manager)
    {
        UserFactory::createMany(5);

        /*$faker = Factory::create('fr_FR');
        for ($users = 0; $users < 10; ++$users) {
            $userEntity = new User();
            $passwordHash = $this->passwordHasher->hashPassword($userEntity, 'password');
            $userEntity
                ->setUsername($faker->userName())
                ->setEmail($faker->email())->setPassword(
                    $passwordHash
                )->setToken($faker->regexify('[A-Z]{20}[0-9]{20}'))
                ->setLastAuthenticationAt(
                    \DateTimeImmutable::createFromMutable($faker->datetime())
                )->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()));
            $manager->persist($userEntity);
        }
        $manager->flush();*/
    }
}
