<?php

namespace App\Factory;

use App\Entity\User;
use App\Repository\UserRepository;
use DateTimeImmutable;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @method static User|Proxy createOne(array $attributes = [])
 * @method static User[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static User|Proxy find($criteria)
 * @method static User|Proxy findOrCreate(array $attributes)
 * @method static User|Proxy first(string $sortedField = 'id')
 * @method static User|Proxy last(string $sortedField = 'id')
 * @method static User|Proxy random(array $attributes = [])
 * @method static User|Proxy randomOrCreate(array $attributes = [])
 * @method static User[]|Proxy[] all()
 * @method static User[]|Proxy[] findBy(array $attributes)
 * @method static User[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static User[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static UserRepository|RepositoryProxy repository()
 * @method User|Proxy create($attributes = [])
 */
final class UserFactory extends ModelFactory
{
    private UserPasswordHasherInterface $userPasswordHasher;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        // TODO inject services if required (https://github.com/zenstruck/foundry#factories-as-services)
        parent::__construct();
        $this->userPasswordHasher = $userPasswordHasher;
    }

    protected static function getClass(): string
    {
        return User::class;
    }

    #[ArrayShape([
        'username' => "string",
        'roles' => "array",
        'password' => "string",
        'email' => "string",
        'token' => "string",
        'lastAuthenticationAt' => "\DateTime",
        'createdAt' => "\DateTime"
    ])]
    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://github.com/zenstruck/foundry#model-factories)
            'username' => self::faker()->userName(),
            'password' => 'password',
            'roles' => [],
            'email' => self::faker()->email(),
            'token' => self::faker()->password(minLength: 20),
            'lastAuthenticationAt' => DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'createdAt' => DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
        ];
    }

    protected function initialize(): self
    {
        // see https://github.com/zenstruck/foundry#initialization
        return $this->afterInstantiate(
                function (User $user) {
                    $user->setPassword($this->userPasswordHasher->hashPassword($user, $user->getPassword()));
                }
            );
    }
}
