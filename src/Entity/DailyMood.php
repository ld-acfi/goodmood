<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DailyMoodRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DailyMoodRepository::class)
 */
#[ApiResource]
class DailyMood
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="dailyMoods")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Mood::class, inversedBy="dailyMoods")
     * @ORM\JoinColumn(nullable=false)
     */
    private $mood;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=DailyActivity::class, mappedBy="dailyMood")
     */
    private $dailyActivities;

    public function __construct()
    {
        $this->dailyActivities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getMood(): ?Mood
    {
        return $this->mood;
    }

    public function setMood(?Mood $mood): self
    {
        $this->mood = $mood;
        return $this;
    }


    public function __toString()
    {
        return $this->getMood();
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Collection|DailyActivity[]
     */
    public function getDailyActivities(): Collection
    {
        return $this->dailyActivities;
    }

    public function addDailyActivity(DailyActivity $dailyActivity): self
    {
        if (!$this->dailyActivities->contains($dailyActivity)) {
            $this->dailyActivities[] = $dailyActivity;
            $dailyActivity->setDailyMood($this);
        }
        return $this;
    }

    public function removeDailyActivity(DailyActivity $dailyActivity): self
    {
        if ($this->dailyActivities->removeElement($dailyActivity)) {
            // set the owning side to null (unless already changed)
            if ($dailyActivity->getDailyMood() === $this) {
                $dailyActivity->setDailyMood(null);
            }
        }
        return $this;
    }

}
