<?php

namespace App\Entity;

use App\Repository\DailyActivityRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *      denormalizationContext={"groups"={"dailyactivity:write"}},
 *       attributes={"pagination_enabled"=false},
 *         itemOperations={
 *               "GET"={
 *                  "openapi_context"={
 *                      "summary" = "Permets de recuperer une activité journalière.",
 *                      "description" = "Permets la récupération de toutes les informations d'une activité journalière en fonction d'un Id.",
 *                  }
 *               },
 *              "DELETE"={
 *                  "method" = "DELETE",
 *                  "path" = "/daily_activities/{id}/delete",
 *                  "openapi_context"={
 *                      "summary" = "Permets de supprimer une activité journalière.",
 *                      "description" = "Permets la suppression d'une activité journalière en fonction d'un Id.",
 *                  }
 *              },
 *              "PUT" = {
 *                  "openapi_context"={
 *                      "summary" = "Permets la mise à jour d'une activité journalière.",
 *                      "description" = "Permets la modification tottale d'une activité journalière en fonction d'un Id.",
 *                  },
 *              
 *              }
 *        },
 *         collectionOperations={
 *              "GET"={
 *                  "openapi_context"={
 *                      "summary" = "Permets de recuperer toutes les activités journalière.",
 *                      "description" = "Permets la récupération de toutes les informations de toutes la collection.",
 *                  }
 *               },
 *              "POST"={
 *                  "method" = "POST",
 *                  "path" = "/daily_activities/create",
 *                  "openapi_context"={
 *                      "summary" = "Permets la creation d'une activité journalière.",
 *                      "description" = "Permets la création d'une activité journlière",
 *                  }
 *              }
 *         }
 * )
 * @ORM\Entity(repositoryClass=DailyActivityRepository::class)
 */

class DailyActivity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=DailyMood::class, inversedBy="dailyActivities")
     * @Groups({"dailyactivity:write"})
     * @Assert\NotBlank(
     *      message = "Le champ ne peut pas etre vide."
     * )
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="/api/daily_moods/{id}"
     *         }
     *      }
     * )
     * 
     */
    private $dailyMood;

    /**
     * @ORM\ManyToOne(targetEntity=Activity::class, inversedBy="dailyActivities")
     * @Groups({"dailyactivity:write"})
     * @Assert\NotBlank(
     *      message = "Le champ ne peut pas etre vide."
     * )
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="/api/activities/{id}"
     *         }
     *      }
     * )
     */
    private $activity;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDailyMood(): ?DailyMood
    {
        return $this->dailyMood;
    }

    public function setDailyMood(?DailyMood $dailyMood): self
    {
        $this->dailyMood = $dailyMood;
        return $this;
    }

    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    public function setActivity(?Activity $activity): self
    {
        $this->activity = $activity;
        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

}

