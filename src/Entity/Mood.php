<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MoodRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MoodRepository::class)
 * @ApiResource()
 */
class Mood
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $rating;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=MoodPicture::class, mappedBy="mood", orphanRemoval=true)
     */
    private $moodPictures;

    /**
     * @ORM\OneToMany(targetEntity=DailyMood::class, mappedBy="mood", orphanRemoval=true)
     */
    private $dailyMoods;

    public function __construct()
    {
        $this->moodPictures = new ArrayCollection();
        $this->dailyMoods = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;
        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection|MoodPicture[]
     */
    public function getMoodPictures(): Collection
    {
        return $this->moodPictures;
    }

    public function addMoodPicture(MoodPicture $moodPicture): self
    {
        if (!$this->moodPictures->contains($moodPicture)) {
            $this->moodPictures[] = $moodPicture;
            $moodPicture->setMood($this);
        }
        return $this;
    }

    public function removeMoodPicture(MoodPicture $moodPicture): self
    {
        if ($this->moodPictures->removeElement($moodPicture)) {
            // set the owning side to null (unless already changed)
            if ($moodPicture->getMood() === $this) {
                $moodPicture->setMood(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|DailyMood[]
     */
    public function getDailyMoods(): Collection
    {
        return $this->dailyMoods;
    }

    public function addDailyMood(DailyMood $dailyMood): self
    {
        if (!$this->dailyMoods->contains($dailyMood)) {
            $this->dailyMoods[] = $dailyMood;
            $dailyMood->setMood($this);
        }
        return $this;
    }

    public function removeDailyMood(DailyMood $dailyMood): self
    {
        if ($this->dailyMoods->removeElement($dailyMood)) {
            // set the owning side to null (unless already changed)
            if ($dailyMood->getMood() === $this) {
                $dailyMood->setMood(null);
            }
        }
        return $this;
    }
}
