<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ActivityPictureRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ActivityPictureRepository::class)
 * @ApiResource(
 *      denormalizationContext={"groups"={"picture:write"}},
 *      attributes={"pagination_enabled"=false},
 *
 *
 *       itemOperations={
 *               "GET"={
 *                  "openapi_context"={
 *                      "summary" = "Permets de recuperer une image.",
 *                      "description" = "Permets la récupération du nom de l'image et du path de l'image en fonction d'un Id.",
 *                  },
 *               },
 *              "DELETE"={
 *                  "method" = "DELETE",
 *                  "path" = "/activity_pictures/{id}/delete",
 *                  "security"="is_granted('ROLE_ADMIN')",
 *                  "security_message"="Vous n'avez pas les droits necessaires pour supprimer cette activité.",
 *                  "openapi_context"={
 *                      "summary" = "Permets de supprimer une image.",
 *                      "description" = "Permets la suppression d'une image en fonction d'un id."
 *                  }
 *              },
 *              "PUT" = {
 *                 "security"="is_granted('ROLE_ADMIN')",
 *                 "security_message"="Vous n'avez pas les droits necessaires pour mettre à jour une image.",
 *                  "openapi_context"={
 *                      "summary" = "Permets la mise à jour d'une image.",
 *                      "description" = "Permets la modification des informations d'une image en fonction de son id."
 *                  }
 *              }
 *        },
 *        collectionOperations={
 *              "GET"={
 *                  "openapi_context"={
 *                      "summary" = "Permets de recuperer toutes les images.",
 *                      "description" = "Permets la récupération de toutes les informations de toutes les images."
 *                  }
 *               },
 *              "POST"={
 *                  "method" = "POST",
 *                  "path" = "/activity_pictures/create",
 *                  "security"="is_granted('ROLE_ADMIN')",
 *                 "security_message"="Vous n'avez pas les droits necessaires pour ajouter une image.",
 *                  "openapi_context"={
 *                      "summary" = "Permets la creation d'une image.",
 *                      "description" = "Permets la creation d'une image."
 *                  }
 *              }
 *         }
 * )
 */
class ActivityPicture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *      message = "Le champ ne peut pas etre vide."
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "Le nom de l'image doit contenir au minimum {{ limit }} characteres",
     *      maxMessage = "Le nom de l'image doit contenir au maximum {{ limit }} characteres"
     * )
     * @Groups({"picture:write"})
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="football"
     *         }
     *      }
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *      message = "Le champ ne peut pas etre vide."
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "Le chemin de l'image doit contenir au minimum {{ limit }} characteres",
     *      maxMessage = "Le chemin de l'image doit contenir au maximum {{ limit }} characteres"
     * )
     * @Groups({"picture:write"})
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="football.jpg"
     *         }
     *      }
     * )
     */
    private $path;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Activity::class, inversedBy="activityPictures")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(
     *      message = "Le champ ne peut pas etre vide."
     * )
     * @Groups({"picture:write"})
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="/api/activities/{id}"
     *         }
     *      }
     * )
     */
    private $activity;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;
        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    public function setActivity(?Activity $activity): self
    {
        $this->activity = $activity;
        return $this;
    }
}
