<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ActivityRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ActivityRepository::class)
 * @ApiResource(
 *        normalizationContext={"groups"={"activity:read"}},
 *        denormalizationContext={"groups"={"activity:write"}},
 *        attributes={"pagination_enabled"=false},
 *         itemOperations={
 *               "GET"={
 *                  "openapi_context"={
 *                      "summary" = "Permets de recuperer une activité.",
 *                      "description" = "Permets la récupération de toutes les informations d'une acticité en fonction d'un Id.",
 *                  }
 *               },
 *              "DELETE"={
 *                  "method" = "DELETE",
 *                  "path" = "/activity/{id}/delete",
 *                  "security"="is_granted('ROLE_ADMIN')",
 *                  "security_message"="Vous n'avez pas les droits necessaires pour supprimer cette activité.",
 *                  "openapi_context"={
 *                      "summary" = "Permets de supprimer une activité.",
 *                      "description" = "Permets la suppression d'une activité en fonction d'un Id.",
 *                  }
 *              },
 *              "PUT" = {
 *                 "security"="is_granted('ROLE_ADMIN')",
 *                 "security_message"="Vous n'avez pas les droits necessaires pour mettre à jour une activité.",
 *                  "openapi_context"={
 *                      "summary" = "Permets la mise à jour d'une activité.",
 *                      "description" = "Permets la modification tottale d'une activité en fonction d'un Id.",
 *                  }
 *              }
 *        },
 *         collectionOperations={
 *              "GET"={
 *                  "openapi_context"={
 *                      "summary" = "Permets de recuperer toutes les activités.",
 *                      "description" = "Permets la récupération de toutes les informations de toutes la collection.",
 *                  }
 *               },
 *              "POST"={
 *                  "method" = "POST",
 *                  "path" = "/activity/create",
 *                  "security"="is_granted('ROLE_ADMIN')",
 *                 "security_message"="Vous n'avez pas les droits necessaires pour ajouter une activité.",
 *                  "openapi_context"={
 *                      "summary" = "Permets la creation d'une activité.",
 *                      "description" = "Permets la création d'une activité",
 *                  }
 *              }
 *         }
 *
 * )
 */
class Activity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"activity:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"activity:read","activity:write"})
     * @Assert\NotBlank(
     *      message = "Le champ ne peut pas etre vide."
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "Votre mot de passe doit contenir au minimum {{ limit }} characteres",
     *      maxMessage = "Votre mot de passe doit contenir au maximum {{ limit }} characteres"
     * )
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="Football"
     *         }
     *      }
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=ActivityPicture::class, mappedBy="activity", orphanRemoval=true)
     *
     */
    private $activityPictures;

    /**
     * @ORM\OneToMany(targetEntity=DailyActivity::class, mappedBy="activity")
     */
    private $dailyActivities;

    public function __construct()
    {
        $this->activityPictures = new ArrayCollection();
        $this->createdAt = new DateTime();
        $this->dailyActivities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Collection|ActivityPicture[]
     */
    public function getActivityPictures(): Collection
    {
        return $this->activityPictures;
    }

    public function addActivityPicture(ActivityPicture $activityPicture): self
    {
        if (!$this->activityPictures->contains($activityPicture)) {
            $this->activityPictures[] = $activityPicture;
            $activityPicture->setActivity($this);
        }
        return $this;
    }

    public function removeActivityPicture(ActivityPicture $activityPicture): self
    {
        if ($this->activityPictures->removeElement($activityPicture)) {
            // set the owning side to null (unless already changed)
            if ($activityPicture->getActivity() === $this) {
                $activityPicture->setActivity(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|DailyActivity[]
     */
    public function getDailyActivities(): Collection
    {
        return $this->dailyActivities;
    }

    public function addDailyActivity(DailyActivity $dailyActivity): self
    {
        if (!$this->dailyActivities->contains($dailyActivity)) {
            $this->dailyActivities[] = $dailyActivity;
            $dailyActivity->setActivity($this);
        }
        return $this;
    }

    public function removeDailyActivity(DailyActivity $dailyActivity): self
    {
        if ($this->dailyActivities->removeElement($dailyActivity)) {
            // set the owning side to null (unless already changed)
            if ($dailyActivity->getActivity() === $this) {
                $dailyActivity->setActivity(null);
            }
        }
        return $this;
    }


    public function __toString()
    {
        return $this->getName();
    }
}
