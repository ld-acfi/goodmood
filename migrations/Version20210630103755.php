<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210630103755 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE daily_activity (id INT AUTO_INCREMENT NOT NULL, daily_mood_id INT DEFAULT NULL, activity_id INT DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', INDEX IDX_167BDE8E676802CA (daily_mood_id), INDEX IDX_167BDE8E81C06096 (activity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE daily_activity ADD CONSTRAINT FK_167BDE8E676802CA FOREIGN KEY (daily_mood_id) REFERENCES daily_mood (id)');
        $this->addSql('ALTER TABLE daily_activity ADD CONSTRAINT FK_167BDE8E81C06096 FOREIGN KEY (activity_id) REFERENCES activity (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE daily_activity');
    }
}
