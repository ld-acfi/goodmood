<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210720134012 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE activity CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE activity_picture CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE daily_activity CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE daily_mood CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE mood CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE mood_picture CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE note CHANGE created_at created_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL, CHANGE last_authentication_at last_authentication_at DATETIME DEFAULT NULL, CHANGE created_at created_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE activity CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE activity_picture CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE daily_activity CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE daily_mood CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE mood CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE mood_picture CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE note CHANGE created_at created_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE last_authentication_at last_authentication_at DATETIME NOT NULL, CHANGE created_at created_at DATETIME NOT NULL');
    }
}
