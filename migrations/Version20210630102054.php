<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210630102054 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE daily_mood (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, mood_id INT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime)\', INDEX IDX_473FB163A76ED395 (user_id), INDEX IDX_473FB163B889D33E (mood_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE daily_mood ADD CONSTRAINT FK_473FB163A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE daily_mood ADD CONSTRAINT FK_473FB163B889D33E FOREIGN KEY (mood_id) REFERENCES mood (id)');
        $this->addSql('DROP TABLE user_activity');
        $this->addSql('DROP TABLE user_mood');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_activity (user_id INT NOT NULL, activity_id INT NOT NULL, INDEX IDX_4CF9ED5A81C06096 (activity_id), INDEX IDX_4CF9ED5AA76ED395 (user_id), PRIMARY KEY(user_id, activity_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE user_mood (user_id INT NOT NULL, mood_id INT NOT NULL, INDEX IDX_79B8E23FA76ED395 (user_id), INDEX IDX_79B8E23FB889D33E (mood_id), PRIMARY KEY(user_id, mood_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_activity ADD CONSTRAINT FK_4CF9ED5A81C06096 FOREIGN KEY (activity_id) REFERENCES activity (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_activity ADD CONSTRAINT FK_4CF9ED5AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_mood ADD CONSTRAINT FK_79B8E23FA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_mood ADD CONSTRAINT FK_79B8E23FB889D33E FOREIGN KEY (mood_id) REFERENCES mood (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('DROP TABLE daily_mood');
    }
}
