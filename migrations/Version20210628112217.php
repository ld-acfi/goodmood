<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210628112217 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mood_picture ADD mood_id INT NOT NULL');
        $this->addSql('ALTER TABLE mood_picture ADD CONSTRAINT FK_28D637BFB889D33E FOREIGN KEY (mood_id) REFERENCES mood (id)');
        $this->addSql('CREATE INDEX IDX_28D637BFB889D33E ON mood_picture (mood_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mood_picture DROP FOREIGN KEY FK_28D637BFB889D33E');
        $this->addSql('DROP INDEX IDX_28D637BFB889D33E ON mood_picture');
        $this->addSql('ALTER TABLE mood_picture DROP mood_id');
    }
}
