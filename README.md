# api

Création de l'API pour l'application mobile mood board.

# Installation

1. Clonez le dépot où vous voulez
2. Créer un fichier dotenv (`.env`)
3. Installez les dépendances : `composer install`
4. Configurer le fichier `.env` en vous référant au fichier `.env.example`
    1. Base de données  
       - Configurer la variable `DATABASE_URL` dans le .env en adaptant le driver selon votre configuration (MySQL, PostgresSQL, etc...)
       - Créez la BDD `php bin/console doctrine:database:create`
    2. API Platform  
    Configurer la variable `CORS_ALLOW_ORIGIN` selon le `.env.example`
5. Jouez les migrations : `php bin/console d:m:m`
6. Lancez le server : `symfony serve` ou `php -S localhost:3000 -t public`
